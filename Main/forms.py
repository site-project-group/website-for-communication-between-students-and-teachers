from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms

from Users.models import User


class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-input'}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-input'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-input'}))
    password2 = forms.CharField(label='Повтор пароля', widget=forms.PasswordInput(attrs={'class': 'form-input'}))
    course = forms.IntegerField(label='Курс', widget=forms.TextInput(attrs={'class': 'form-input'}))
    group = forms.CharField(label='Группа', max_length=50, widget=forms.TextInput(attrs={'class': 'form-input'}))
    role = forms.CharField(label='Роль', max_length=20, widget=forms.TextInput(attrs={'class': 'form-input'}))
    faculty = forms.CharField(label='Факультет', max_length=100, widget=forms.TextInput(attrs={'class': 'form-input'}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'course', 'group', 'role', 'faculty')


class LoginUserForm(AuthenticationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-input'}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-input'}))

