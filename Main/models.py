from django.db import models

from Users.models import User


class Tasks(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    user_from = models.IntegerField()
    user_to = models.IntegerField()
    title = models.CharField(max_length=100)
    text = models.TextField()
    date = models.DateField(auto_now_add=True, null=True)
    file = models.FileField(upload_to='files/', null=True)
    filename = models.CharField(max_length=100, null=True)
    status = models.CharField(max_length=100, default='Не прочитано')
    deadline = models.DateField(null=True)

class Messages(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    to_id = models.IntegerField()
    from_id = models.IntegerField()
    created_at = models.DateTimeField()
    room = models.IntegerField()
    isReaded = models.BooleanField()


class Content(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    message_id = models.ForeignKey("Messages", related_name="message_text", on_delete=models.PROTECT)
    content = models.TextField()


class UserFollowing(models.Model):
    user_id = models.ForeignKey(User, related_name="following", on_delete=models.CASCADE)
    following_user_id = models.ForeignKey(User, related_name="followers", on_delete=models.CASCADE)
