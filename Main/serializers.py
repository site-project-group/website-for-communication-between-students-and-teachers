from rest_framework import serializers
from Users.models import User
from .models import Tasks



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username',)

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ('user_to', 'user_from', 'title', 'text')