document.addEventListener('DOMContentLoaded', () => {
 
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

    //подключает к комнате, лежит в task.py
    socket.emit('joiner', {'room': 'tasks'});

    //уведомление о новом таске
    socket.on('new_task', data => {

        if (data) {
            const div = document.createElement('div');
            const span = document.createElement('span');
            const p = document.createElement('p');

            div.className = "notification";
            span.className = "closebtn";
            p.id = "noti";

            span.innerHTML = 'х';
            p.innerHTML = `Пользователь ${data.name} создал вам таск "${data.title}"`;
            div.innerHTML += span.outerHTML + p.outerHTML;
            document.querySelector('#cringe').append(div);

            const closer = document.querySelectorAll('.closebtn');

            closer.forEach((item) => {
                item.addEventListener("click", click, false);
            });
        };
        
    });

    //уведомление о новом сообщении
    socket.on('message', data => {
        if (data) {
            const div = document.createElement('div');
            const span = document.createElement('span');
            const p = document.createElement('p');

            div.className = "notification";
            span.className = "closebtn";
            p.id = "noti";

            span.innerHTML = 'х';
            p.innerHTML = `Пользователь ${data.user} отправил вам сообщение по теме "${data.title}"`;
            div.innerHTML += span.outerHTML + p.outerHTML;
            document.querySelector('#cringe').append(div);

            const closer = document.querySelectorAll('.closebtn');

            closer.forEach((item) => {
                item.addEventListener("click", click, false);
            });
        };
    });

    //закрывает уведомлени, обрабатывая событие click
    function click() {
        let div = this.parentElement;
        
        div.style.opacity = "0";
        setTimeout(function(){ div.style.display = "none"; }, 600);
    };   
})