
document.addEventListener('DOMContentLoaded', () => {

    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

    const user = document.querySelector('#get-user').innerHTML;
    let user_to = document.querySelector('#get-user').innerHTML;

    let room = document.querySelector('.default-room').id;
    joinRoom(room);

    
    document.querySelector('#send_message').onclick = () => {
        
        socket.emit('incoming-message', {'message': utf8_to_b64(document.querySelector('#user_message').value),
            'user': user, 'room': room, 'user_to': user_to});

        document.querySelector('#user_message').value = '';
    };


    socket.on('message', data => {

        if (data.message) {
            const div = document.createElement('div')
            
            const span_user = document.createElement('span');
            const span_time = document.createElement('span');
            
            if (data.user == user) {
                
                    div.setAttribute("class", "my-msg");
                    const br = document.createElement('br');

                    span_user.setAttribute("class", "my-username");
                    span_user.innerText = "Вы";
                    
                    span_time.setAttribute("class", "timestamp");
                    span_time.innerText = data.time_stamp;

                    let mess = data.message.split('\n');
                    div.innerHTML += span_user.outerHTML + br.outerHTML
                    mess.forEach((item) => {
                        const p = document.createElement('p');
                        p.innerText = item;
                        div.innerHTML += p.outerHTML;
                    });
                    
                    div.innerHTML += span_time.outerHTML

                    document.querySelector('#display-message-section').append(div);
                    document.querySelector('#display-message-section').append(br);

            }
            else if ((data.room != room) & (typeof data.room !== 'undefined')){
                let span = document.querySelector(`.m${data.room}`);

                span.style.fontWeight = "Bold";
                span.innerHTML = data.message;
            }
            else if (typeof data.user !== 'undefined') {

                div.setAttribute("class", "others-msg");
                const br = document.createElement('br');

                span_user.setAttribute("class", "other-username");
                span_user.innerText = data.user;

                span_time.setAttribute("class", "timestamp");
                span_time.innerText = data.time_stamp;

                let mess = data.message.split('\n');
                div.innerHTML += span_user.outerHTML + br.outerHTML

                mess.forEach((item) => {
                    const p = document.createElement('p');
                    p.innerText = item;
                    div.innerHTML += p.outerHTML;
                });
                
                div.innerHTML += span_time.outerHTML
                
                document.querySelector('#display-message-section').append(div);
                document.querySelector('#display-message-section').append(br);
            }

            else {
                printSysMsg(data.message);
            }


        }
        scrollDownChatWindow();
    });

    socket.on('last_message', data => {

        let span = document.querySelector(`.m${data.room}`);
        let inSpan = document.createElement('span');
        
        if (data.you){
            span.style.fontWeight = "Bold";
            inSpan.className = "you";
            inSpan.innerHTML = "Вы: ";
            span.innerHTML = inSpan.outerHTML + data.message;
        }else{
            span.style.fontWeight = "Bold";
            span.innerHTML = data.message;
        };
    });

    socket.on('read_message', data => {
        
        let span = document.querySelector(`.m${data.room}`);
        span.style.fontWeight = "";

    });

    document.querySelectorAll('.select-room').forEach(div => {
        div.addEventListener("click", click, false);});

    function click() {
        let p = this.querySelector('p')
        let newRoom = p.id
        let new_user_to = p.className

        if (newRoom === room) {
            //уведомление о присоединении к комнате
        } else {
            leaveRoom(room);
            joinRoom(newRoom);
            room = newRoom;
            user_to = new_user_to;
        }
    };

    
    function leaveRoom(room) {

        document.querySelectorAll('.select-room').forEach(p => {
            p.style.color = "black";
        });
    }


    function joinRoom(room) {

        socket.emit('join', {'user': user, 'room': room});

        document.querySelector('#display-message-section').innerHTML = '';
        
        let par = document.getElementById(`${room}`);
        let sp = par.querySelector('#message');
        let you = par.querySelector('.you');

        if ((sp != null) & (you == null)){
 
        let span = document.querySelector(`.m${room}`);
        
        span.style.fontWeight = "";
    };

        document.querySelector("#user_message").focus();
    }


    function scrollDownChatWindow() {
        const chatWindow = document.querySelector("#display-message-section");
        chatWindow.scrollTop = chatWindow.scrollHeight;
    }

    function printSysMsg(message) {
        const p = document.createElement('p');
        p.setAttribute("class", "system-msg");
        p.innerHTML = message;
        document.querySelector('#display-message-section').append(p);
        scrollDownChatWindow()

        document.querySelector("#user_message").focus();
    }

    function utf8_to_b64(str) {
        return window.btoa(unescape(encodeURIComponent(str)));
    };
    
    function b64_to_utf8(str) {
        return decodeURIComponent(escape(window.atob(str)));
    };
});


