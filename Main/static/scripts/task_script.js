document.addEventListener('DOMContentLoaded', () => {

    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

    socket.emit('joiner', {'room': 'tasks'});

    // создание блока с новым таском 
    socket.on('new_task', data => {
        
        if (data.id) {
            const div = document.createElement('div');
            const pInfo = document.createElement('p');
            const pStat = document.createElement('p');
            const a = document.createElement('a');

            a.href = `/tasks/${data.id}`;
            pInfo.id = "info";
            pStat.id = "stat";
            div.id = "one";

            if (data.deadline_time){
                pInfo.innerHTML = data.name + ' | ' + data.faculty + ' | ' + data.title + ' | ' + data.deadline_time;
                pStat.innerHTML = data.status;
                div.innerHTML += pInfo.outerHTML + pStat.outerHTML;
                a.innerHTML += div.outerHTML;
                document.querySelector('#deadline_tasks').append(a);
            } else {
                pInfo.innerHTML = data.name + '|' + data.faculty + '|' + data.title;
                pStat.innerHTML = data.status;
                div.innerHTML += pInfo.outerHTML + pStat.outerHTML;
                a.innerHTML += div.outerHTML;
                document.querySelector('#unlimited_tasks').append(a);
            };

        };
    });

    //уведомление о новом сообщении
    socket.on('message', data => {
        if (data) {
            const div = document.createElement('div');
            const span = document.createElement('span');
            const p = document.createElement('p');

            div.className = "notification";
            span.className = "closebtn";
            p.id = "noti";

            span.innerHTML = 'х';
            p.innerHTML = `Пользователь ${data.user} отправил вам сообщение по теме "${data.title}"`;
            div.innerHTML += span.outerHTML + p.outerHTML;
            document.querySelector('#cringe').append(div);

            const closer = document.querySelectorAll('.closebtn');

            closer.forEach((item) => {
                item.addEventListener("click", click, false);
            });
        };
    });

    //закрытие уведомления
    function click() {
        let div = this.parentElement;
        
        div.style.opacity = "0";
        setTimeout(function(){ div.style.display = "none"; }, 600);
    };   
})