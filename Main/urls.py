from django.urls import path
from .views import *

urlpatterns = [
    path('', profile, name='profile'),
    path('friends/', friends, name='friends'),
    path('create_message/', create_message, name='create_message'),
    path('tasks/', TaskAPI.as_view(), name='tasks'),
    path('chat/', chat, name='chat'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('regupdate/', RegisterMyUser.as_view(), name='regupdate'),

]



