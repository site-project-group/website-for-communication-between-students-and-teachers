from django.db.models import Count

from .models import *

menu = [{'id':'1','title': "Профиль", 'url_name': 'profile', 'icon':'image/profile.png'},
        {'id':'2','title': "Друзья", 'url_name': 'friends', 'icon':'image/friends.png'},
        {'id':'3','title': "Создание собщения", 'url_name':'create_message', 'icon':'image/plus.png'},
        {'id':'4','title': "Задачи", 'url_name': 'tasks', 'icon':'image/tasks.png'},
        {'id':'5','title': "Чат", 'url_name': 'chat', 'icon':'image/chat.png'},
        {'id':'6','title': "Выход", 'url_name': 'logout', 'icon':'image/logout.png'},

]

class DataMixin:
    paginate_by = 2

    def get_user_context(self, **kwargs):
        context = kwargs

        user_menu = menu.copy()
        if not self.request.user.is_authenticated:
            user_menu.pop(1)

        context['menu'] = user_menu

        return context