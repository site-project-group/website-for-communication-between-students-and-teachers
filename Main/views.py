from django.contrib.auth import logout, login
from django.contrib.auth.views import LoginView
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.context_processors import request
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.forms import model_to_dict
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from Users.models import User
from Main.serializers import UserSerializer, TaskSerializer

from .forms import *
from .models import *
from .utils import *


def profile(request):
    return render(request, 'main/profile.html', {'menu': menu, 'user': User})


def friends(request):
    return HttpResponse("friends")


def create_message(request):
    return HttpResponse("create_message")


def chat(request):
    return HttpResponse("chat")


class TaskAPI(APIView):

    def get(self, request):
        tasks = Tasks.objects.all().values()
        return Response({'tasks': list(tasks)})

    def post(self, request):
        new_task = Tasks.objects.create(
            title=request.data['title'],
            user_to=request.data['user_to'],
            user_from=request.data['user_from'],
            text=request.data['text']
        )
        return Response({'task': model_to_dict(new_task)})


# class for register users
class RegisterUser(DataMixin, CreateView):
    form_class = RegisterUserForm
    template_name = 'main/register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Регистрация")
        return dict(list(context.items()) + list(c_def.items()))
 
    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('regupdate')


# class for add another data for users
class RegisterMyUser(DataMixin, CreateView):
    form_class = RegisterUserForm
    template_name = 'main/regupdate.html'
    success_url = reverse_lazy('login')

    def get_context_data1(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="ДопРегистрация")
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        return redirect('profile')


class LoginUser(DataMixin, LoginView):
    form_class = LoginUserForm
    template_name = 'main/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Авторизация")
        return dict(list(context.items()) + list(c_def.items()))

    def get_success_url(self):
        return reverse_lazy('profile')


def logout_user(request):
    logout(request)
    return redirect('login')


class APIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
